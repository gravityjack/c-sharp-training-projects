using System.ComponentModel.DataAnnotations;

namespace TicTacToeAPI.DTOs
{
    public class UserIncomingDto
    {
        [Required, EmailAddress]
        public string Email { get; set; }
        
        [Required]
        public string Password{ get; set; }
    }

    public class UserOutgoingDto
    {
        public long Id { get; set; }
        public string Email { get; set; }
    }
}