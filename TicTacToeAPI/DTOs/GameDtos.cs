using TicTacToeAPI.Constants;
using System.ComponentModel.DataAnnotations;

namespace TicTacToeAPI.DTOs
{
    public class GameIncomingDto
    {
        [Required, RegularExpression(GameConstants.XUser + "|" + GameConstants.OUser)]
        public char UserChar { get; set; }
    }

    public class GameOutgoingDto
    {
        public long Id { get; set; }
        public string GameString { get; set; }
        public char UserChar { get; set; }
        public string GameState { get; set; }
    }
}