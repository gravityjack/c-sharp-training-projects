using System.ComponentModel.DataAnnotations;
using TicTacToeAPI.Constants;

namespace TicTacToeAPI.DTOs
{
    public class MoveDto
    {
        [Required, Range(0, GameConstants.NumberOfCells/GameConstants.RowColSize - 1)]
        public int XMove { get; set; }

        [Required, Range(0, GameConstants.NumberOfCells/GameConstants.RowColSize - 1)]
        public int YMove { get; set; }

        public override string ToString()
        {
            return $"{XMove}, {YMove}";
        } 
    }
}