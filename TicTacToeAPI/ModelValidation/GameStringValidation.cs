using System;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using TicTacToeAPI.Models;
using TicTacToeAPI.Constants;

namespace TicTacToeAPI.Validation
{
    public class GameStringValidation : ValidationAttribute
    {
        public static readonly string RegularPattern = "^[" + GameConstants.XUser + "|" + GameConstants.OUser + "|" + GameConstants.EmptyCell + "]{9}$";
        public static readonly string InitialPattern = "^" +GameConstants.EmptyCell +"{9}$";
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            var gameString = (String) value;
            var game = (Game) context.ObjectInstance;

            var pattern = RegularPattern;
            
            if (game.Id == 0)
            {
                pattern = InitialPattern;
            }

            var match = Regex.Match(gameString, pattern);

            if (match.Success)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult($"regular game string must match " + pattern);
        }

    }
}