using TicTacToeAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TicTacToeAPI.Services
{
    public interface IUserService
    {
        Task<User> Authenticate(string username, string password);
        Task<List<User>> GetAll();
        Task<User> GetById(long id);
        Task<User> CreateNew(string username, string password);
        Task<bool> Delete(long id);
        Task<bool> Update(long id, string username, string newpassword);
    }
}