using System.Collections.Generic;
using TicTacToeAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using TicTacToeAPI.Services;
using TicTacToeAPI.DTOs;
using AutoMapper;
using System.Threading.Tasks;

namespace TicTacToeAPI.Services
{
    public class UserService : IUserService
    {
        private readonly TicTacToeContext _context;

        public UserService(TicTacToeContext context)
        {
            _context = context;
        }
        
        public async Task<List<User>> GetAll()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User> GetById(long id)
        {
            return await _context.Users.FindAsync(id);
        }

        public async Task<bool> Delete(long id)
        {
            var user = await GetById(id);

            if (user == null)
            {
                return false;
            }

            await _context.Entry(user).Collection("Games").LoadAsync();
            foreach(Game game in user.Games)
            {
                _context.Games.Remove(game);
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Update(long id, string username, string newpassword)
        {
            var user = await GetById(id);

            if (user == null || !user.Email.Equals(username))
            {
                return false;
            }

            user.Password = newpassword;

            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();

            return true;
        }

        public async Task<User> Authenticate(string username, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email.Equals(username));

            if (user == null)
            {
                return null;
            }

            if (!user.Password.Equals(password))
            {
                return null;
            }

            return user;
        }

        public async Task<User> CreateNew(string username, string password)
        {
            if(string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            {
                return null;
            }

            var user = await _context.Users.FirstOrDefaultAsync( u => u.Email.Equals(username));

            if (user != null)
            {
                return null;
            }

            var newUser = new User{Email = username, Password = password};

            _context.Users.Add(newUser);
            await _context.SaveChangesAsync();

            return newUser;
        }
    }
}