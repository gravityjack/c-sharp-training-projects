using System;
using TicTacToeAPI.Constants;
using System.ComponentModel.DataAnnotations;
using TicTacToeAPI.Validation;
using System.Text.RegularExpressions;
using TicTacToeAPI.DTOs;

namespace TicTacToeAPI.Models
{
    public class Game
    {
       
        public long Id { get; set; }

        [GameStringValidation]
        public string GameString { get; set; } = new String(GameConstants.EmptyCell[0], GameConstants.NumberOfCells);

        [Required, RegularExpression(GameConstants.XUser + "|" + GameConstants.OUser)]
        public char UserChar { get; set; }
        
        [GameStateValidation]
        public string GameState { get; set; } = GameConstants.KeepPlaying;
    
        public virtual User User { get; set; }

        public bool MakeUserMove(MoveDto move)
        {
            return MakeMove(move, UserChar);
        }

        protected bool MakeMove(MoveDto move, char player)
        {
            int index = move.YMove * GameConstants.RowColSize + move.XMove;

            if (GameString[index] != GameConstants.EmptyCell[0])
            {
                return false;
            }

            var charArray = GameString.ToCharArray();
            charArray[index] = player;
            GameString = new string(charArray);

            CheckForWin(player);

            return true;
        }

        protected void CheckForWin(char player)
        {
            //horiz
            string pattern = $"^(.{{{GameConstants.RowColSize}}}){{0,{GameConstants.RowColSize-1}}}{player}{{{GameConstants.RowColSize}}}";
            bool horizWin = Regex.Match(GameString, pattern).Success;
            Console.WriteLine(pattern);
            Console.WriteLine(horizWin);
            //vert
            pattern = $"({player}.{{{GameConstants.RowColSize-1}}}){{{GameConstants.RowColSize-1}}}{player}";
            bool vertWin = Regex.Match(GameString, pattern).Success;

            //diag
            pattern = $"({player}.{{{GameConstants.RowColSize}}}){{{GameConstants.RowColSize-1}}}{player}";
            bool leftDiagWin = Regex.Match(GameString, pattern).Success;
            pattern = $".{{{GameConstants.RowColSize-1}}}{player}(.{{{GameConstants.RowColSize-2}}}{player}){{{GameConstants.RowColSize-1}}}.{{{GameConstants.RowColSize-1}}}";
            bool rightDiagWin = Regex.Match(GameString, pattern).Success;

            if (horizWin || vertWin || leftDiagWin || rightDiagWin)
            {
                SetWinner(player);
            }

            if (GameString.IndexOf(GameConstants.EmptyCell) == -1)
            {
                GameState = GameConstants.Tie;
            }
        }

        protected void SetWinner(char player)
        {
            var newState = GameConstants.XWins;

            if (player == GameConstants.OUser[0])
            {
                newState = GameConstants.OWins;
            }

            GameState = newState;
        }

        public void MakeAIMove()
        {
            if (GameState != GameConstants.KeepPlaying)
            {
                return;
            }

            int index = GameString.IndexOf(GameConstants.EmptyCell);
            var move = new MoveDto();
            move.YMove = index / GameConstants.RowColSize;
            move.XMove = index % GameConstants.RowColSize;

            MakeMove(move, GetAIChar());
        }

        protected char GetAIChar()
        {
            char ai = GameConstants.XUser[0];

            if (UserChar == GameConstants.XUser[0])
            {
                ai = GameConstants.OUser[0];
            }

            return ai;
        }

        public bool Over()
        {
            return GameState != GameConstants.KeepPlaying;
        }
    }
}