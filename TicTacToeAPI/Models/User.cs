using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace TicTacToeAPI.Models
{
    public class User
    {
        public long Id { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public virtual ICollection<Game> Games { get; set; }
    }
}