using AutoMapper;
using TicTacToeAPI.DTOs;
using TicTacToeAPI.Models;


namespace TicTacToeAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<GameIncomingDto, Game>();
            CreateMap<Game, GameOutgoingDto>();
            CreateMap<UserIncomingDto, User>();
            CreateMap<User, UserOutgoingDto>();
        }
    }
}