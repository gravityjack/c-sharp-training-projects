using System;

namespace TicTacToeAPI.Constants
{
    public class GameConstants
    {
        public const string XWins = "X Wins";
        public const string OWins = "O Wins";
        public const string KeepPlaying = "Keep Playing"; 
        public const string Tie = "Tie Game";
        public const string XUser = "X";
        public const string OUser = "O";
        public const string EmptyCell = "-";
        public const int NumberOfCells = 9;
        public const int RowColSize = 3;
    }
}