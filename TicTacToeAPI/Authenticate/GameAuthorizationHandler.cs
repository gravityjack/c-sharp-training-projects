
using TicTacToeAPI.Models;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using System.Security.Claims;
using System;

namespace TicTacToeAPI.Authentication
{
    public class GameAuthorizationHandler : AuthorizationHandler<GameUserRequirement, Game>
    {
        public static readonly string PolicyName = "UserGamePolicy";

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, GameUserRequirement requirement, Game resource)
        {
            var authenticatedId = GetUserId((ClaimsIdentity) context.User.Identity);
        
            if (authenticatedId == resource.User.Id)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }

        public static long GetUserId(ClaimsIdentity identity)
        {
            return Convert.ToInt64(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
        }
    }

    public class GameUserRequirement : IAuthorizationRequirement {}

}