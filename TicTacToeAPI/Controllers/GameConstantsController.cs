using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToeAPI.Models;
using TicTacToeAPI.Constants;

namespace TicTacToeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameConstantsController : ControllerBase
    {

        [HttpGet("GameStates")]
        public ActionResult<string[]> GetGameStates()
        {
            return new string[] {GameConstants.KeepPlaying, GameConstants.XWins, GameConstants.OWins};
        }

        [HttpGet("ValidUserChars")]
        public ActionResult<string[]> GetValidUserChars()
        {
            return new string[] {GameConstants.XUser, GameConstants.OUser};
        }

        [HttpGet("EmptyCellChar")]
        public ActionResult<string> GetEmptyCellChar()
        {
            return GameConstants.EmptyCell;
        }

    }
}