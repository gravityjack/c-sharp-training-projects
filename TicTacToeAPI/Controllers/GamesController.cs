using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicTacToeAPI.Models;
using TicTacToeAPI.Services;
using TicTacToeAPI.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System;
using System.Threading.Tasks;
using TicTacToeAPI.Authentication;

namespace TicTacToeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private readonly TicTacToeContext _context;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IAuthorizationService _authService;

        public GamesController(TicTacToeContext context, IMapper mapper, IUserService userService, IAuthorizationService authService)
        {
            _context = context;
            _mapper = mapper;
            _userService = userService;
            _authService = authService;
        }

        [Authorize]
        [HttpGet]
        public ActionResult<IEnumerable<GameOutgoingDto>> GetAllGames()
        {
            long userId = GameAuthorizationHandler.GetUserId((ClaimsIdentity)User.Identity);
            var games = _context.Games.Where(g => g.User.Id == userId).ToList();
            List<GameOutgoingDto> dtoList = _mapper.Map<List<Game>, List<GameOutgoingDto>>(games);
            return dtoList;
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<GameOutgoingDto>> GetGameById(long id)
        {
            var game = await _context.Games.FindAsync(id);

            if (game == null)
            {
                return NotFound();
            }

            _context.Entry(game).Reference("User").Load();
            var authResult = await _authService.AuthorizeAsync(User, game, GameAuthorizationHandler.PolicyName);

            if (!authResult.Succeeded)
            {
                return Unauthorized();
            }

            var dto = _mapper.Map<GameOutgoingDto>(game);
            return dto;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<GameOutgoingDto>> PostGame(GameIncomingDto gameDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var game = _mapper.Map<Game>(gameDto);
            game.User = await GetUser();
            _context.Games.Add(game);
            await _context.SaveChangesAsync();

            var dto = _mapper.Map<GameOutgoingDto>(game);

            return CreatedAtAction(nameof(GetGameById), new { id = dto.Id }, dto);            
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult<GameOutgoingDto>> UpdateGame(long id, MoveDto newMove)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
                
            var game = await _context.Games.FindAsync(id);

            if (game == null)
            {
                return NotFound();
            }

           _context.Entry(game).Reference("User").Load();
            var authResult = await _authService.AuthorizeAsync(User, game, GameAuthorizationHandler.PolicyName);

            if (!authResult.Succeeded)
            {
                return Unauthorized();
            }


            if (!game.Over())
            {
                if (!game.MakeUserMove(newMove))
                {
                    return BadRequest(newMove.ToString() + " is already occupied");
                }

                game.MakeAIMove();
                
                _context.Entry(game).State = EntityState.Modified;
                _context.SaveChanges();
            }
            var dto = _mapper.Map<GameOutgoingDto>(game);

            return CreatedAtAction(nameof(GetGameById), new { id = dto.Id }, dto);      
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteGame(long id)
        {
            var game = await _context.Games.FindAsync(id);

            if (game == null)
            {
                return NotFound();
            }
            
            _context.Entry(game).Reference("User").Load();
            var authResult = await _authService.AuthorizeAsync(User, game, GameAuthorizationHandler.PolicyName);

            if (!authResult.Succeeded)
            {
                return Unauthorized();
            }

            
            _context.Games.Remove(game);
            _context.SaveChanges();

            return NoContent();
        }
        
        protected async Task<User> GetUser()
        {
            long id = GameAuthorizationHandler.GetUserId((ClaimsIdentity)User.Identity);
            var user = await _userService.GetById(id);
            return user;
        }

       
    }
    
}