using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicTacToeAPI.Models;
using TicTacToeAPI.Services;
using TicTacToeAPI.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace TicTacToeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IUserService service, IMapper mapper)
        {
            _userService = service;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<UserOutgoingDto>>> GetAllUsers()
        {
            var userList = await _userService.GetAll();
            List<UserOutgoingDto> dtoList = _mapper.Map<List<User>, List<UserOutgoingDto>>(userList);
            return dtoList;
        }

        [HttpPost]
        public async Task<ActionResult<UserOutgoingDto>> CreateUser(UserIncomingDto userDto)
        {
            var newUser = await _userService.CreateNew(userDto.Email, userDto.Password);

            if (newUser == null)
            {
                return BadRequest($"{userDto.Email} exists.");
            }

            var outgoingDto = _mapper.Map<UserOutgoingDto>(newUser);
            return CreatedAtAction(nameof(GetUserById), new { id = outgoingDto.Id }, outgoingDto);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult<UserOutgoingDto>> ChangePassword(long id, UserIncomingDto incomingDto)
        {
            var success = await _userService.Update(id, incomingDto.Email, incomingDto.Password);

            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<UserOutgoingDto>> GetUserById(long id)
        {
            var user = await _userService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            var outgoingDto = _mapper.Map<UserOutgoingDto>(user);
            return outgoingDto;
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteUser(long id)
        {
            var success = await _userService.Delete(id);

            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }
    }

} 